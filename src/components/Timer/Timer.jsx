import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as timeActions from '../../actions/timeActions';

import { Navigation, HeaderRow, FABButton, Icon, Grid, Cell, Card, CardTitle, CardActions, Button } from 'react-mdl';

const styles = {
  FABButtonZPlay: {
    position: 'fixed',
    display: 'block',
    right: '0',
    bottom: '0',
    marginRight: '40px',
    marginBottom: '40px',
    zIndex: '900'
  },
  FABButtonStop: {
    position: 'fixed',
    display: 'block',
    right: '0',
    bottom: '0',
    marginRight: '50px',
    marginBottom: '110px',
    zIndex: '900'
  },
  FABButtonReplay: {
    position: 'fixed',
    display: 'block',
    right: '0',
    bottom: '0',
    marginRight: '50px',
    marginBottom: '165px',
    zIndex: '900'
  }
}

var Time;

class Timer extends Component {

  constructor(props){
    super(props);

    this.state = {
      seconds: 0,
      nameIcon: 'play_arrow',
      pause: false,
      replay: false,
      stop: false,
      play: false,
      timer: '0 : 0 : 0'
    }

    this.handlePlay = this.handlePlay.bind(this);
    this.handlePause = this.handlePause.bind(this);
    this.handleStop = this.handleStop.bind(this);
    this.handleReplay = this.handleReplay.bind(this);
    this.handleTick = this.handleTick.bind(this);
    this.renderFABButtonReplay = this.renderFABButtonReplay.bind(this);
    this.renderCarTime = this.renderCarTime.bind(this);

  }

  componentWillMount(){
    //horas * 3600 + minutos * 60 + segundos
    const tiempoarestar = ( this.props.item.hours * 3600)  + ( this.props.item.minutes  * 60) + this.props.item.seconds;

    let numhours = Math.floor((tiempoarestar % 86400) / 3600); // horas
    let numminutes = Math.floor(((tiempoarestar % 86400) % 3600) / 60); // minutos
    let numseconds = ((tiempoarestar % 86400) % 3600) % 60; // segundos

    let result = `${numhours} : ${numminutes} : ${numseconds}`

    this.setState({
      seconds: tiempoarestar,
      timer: result
    })

    this.props.actions.loadTimeItems();
  }

  // time of executing a task
  handleTick () {

      let seconds = this.state.seconds--;

      //let numdays = Math.floor(seconds / 86400);// dias
      let numhours = Math.floor((seconds % 86400) / 3600); // horas
      let numminutes = Math.floor(((seconds % 86400) % 3600) / 60); // minutos
      let numseconds = ((seconds % 86400) % 3600) % 60; // segundos

      let result = `${numhours} : ${numminutes} : ${numseconds}`

      console.log(result);

      this.setState({
        timer: result
      })

  }

  handlePlay() {
    clearInterval(Time);
    Time = setInterval(this.handleTick, 1000);
    this.setState({nameIcon: 'pause', play: true, pause: false, stop: false, replay: false })
    console.log('Play', this.props.item.description)
    this.props.actions.loadTimeItems();
  }

  handlePause () {
    clearInterval(Time);
    this.setState({nameIcon: 'play_arrow', pause: true, play: false })
    console.log(`se pauso en el tiempo ${Time} mas el tiempo trascurrido ${this.state.seconds}`)
  }

  handleStop(){
    clearInterval(Time);
    this.setState({ nameIcon: 'play_arrow', stop: true, play: false })
    console.log('stop', Time)
  }

  handleReplay(){
    clearInterval(Time);
    tiempoarestar = (1 * 3600)  + (2 * 60) + 4;
    this.setState({ nameIcon: 'play_arrow', replay: true, play: true })
    console.log('se reinicio')
  }


  renderFABButtonStop () {
    if( this.state.nameIcon === 'pause'){
      return <FABButton mini colored ripple style={styles.FABButtonReplay}
        onClick={ this.handleReplay }>
          <Icon name='replay' />
      </FABButton>;
    }
  }

  renderFABButtonReplay () {
    if( this.state.nameIcon === 'pause'){
      return <FABButton mini colored ripple style={styles.FABButtonStop}
        onClick={ this.handleStop }>
          <Icon name='stop' />
      </FABButton>;
    }
  }

  renderCarTime () {
    if( this.props.item ) {
      return <Grid >
        <Cell col={12}>
          <Card shadow={0} style={{width: '500px', height: '200px', margin: 'auto'}}>
             <CardTitle expand style={{ color: '#000000'}}>
                 <Grid>
                   <Cell col={12}>
                     <p style={{textAlign: 'center', margin: '0'}}>
                       {this.props.item.title}
                     </p>
                   </Cell>
                   <Cell col={12}>
                     <h1 style={{textAlign: 'center', margin: '0'}}>{this.state.timer}</h1>
                   </Cell>
                 </Grid>
             </CardTitle>
         </Card>
        </Cell>
      </Grid>
    }
  }

  render() {

    return(

        <div>

          { this.renderCarTime() }

          <FABButton colored ripple style={styles.FABButtonZPlay}
           onClick={ this.state.play === false ? this.handlePlay : this.handlePause }>
             <Icon name={this.state.nameIcon} />
          </FABButton>

             { this.renderFABButtonStop() }
             { this.renderFABButtonReplay() }

         </div>

    );
  }

}

Timer.propTypes = {
  item: PropTypes.object
}

function mapStateToProps (state) {
  return {
    item: state.time.item
  }
}

function mapDispatchToProps (dispatch) {
  return {
    actions: bindActionCreators(timeActions, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Timer);
