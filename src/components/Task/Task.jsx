import React from 'react';
import PropTypes from 'prop-types';
import { Content as Content_mdl , Grid, Cell, Card, CardTitle, CardText, Button, CardActions, Icon, FABButton, CardMenu, IconButton, Menu, MenuItem } from 'react-mdl';


const Task = ({
    id,
    title,
    description,
    seconds,
    minutes,
    hours,
    finished,
    time_execution,
    onAddItem
  }) => (

    <Cell col={4}>
      <Card shadow={0} style={{width: 'auto', height: 'auto', background: '#3E4EB8', margin: 'auto'}}>
         <CardTitle expand style={{alignItems: 'flex-start', color: '#fff'}}>
             <Grid>
               <Cell col={12}>
                 <p className="Title_Task">
                   {title}
                 </p>
               </Cell>
               <Cell col={12}>
                 <p className="Description_Task">
                   {description}
                 </p>
               </Cell>
             </Grid>
         </CardTitle>
         <CardActions border style={{borderColor: 'rgba(255, 255, 255, 0.2)', display: 'flex', boxSizing: 'border-box', alignItems: 'center', color: '#fff'}}>
             <Button colored style={{color: '#fff'}}>Add to Calendar</Button>
             <div className="mdl-layout-spacer"></div>
             <FABButton mini
               onClick={() => onAddItem({ id, title, description, seconds, minutes, hours, finished, time_execution }) }>
                 <Icon name="play_arrow" />
             </FABButton>
         </CardActions>
         <CardMenu style={{color: '#fff'}}>
             <IconButton name="more_vert"  id="nosabemos" />
             <Menu target="nosabemos" align="right">
                 <MenuItem>Editar</MenuItem>
                 <MenuItem>Eliminar</MenuItem>
                 <MenuItem disabled>Otro</MenuItem>
             </Menu>
         </CardMenu>
     </Card>
    </Cell>


);

Task.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  seconds: PropTypes.number.isRequired,
  minutes: PropTypes.number.isRequired,
  hours: PropTypes.number.isRequired,
  finished: PropTypes.bool,
  time_execution: PropTypes.number,
  onAddItem: PropTypes.func
}

export default Task;
