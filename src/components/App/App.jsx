import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import _ from 'underscore';
import { Layout, Navigation, Drawer } from 'react-mdl';

// Components
import Header from '../../common/Header';

class App extends Component {

  render() {

    return(
        <Layout fixedHeader fixedDrawer>
            <Header />
            <Drawer title="Adinistrador">
                <Navigation>
                    <a href="#">Ver Grafica</a>
                    <a href="#">Lista de tareas</a>
                </Navigation>
            </Drawer>

            {this.props.children}

        </Layout>

    )
  }
}

App.PropTypes = {
  children: PropTypes.object.isRequired
};

export default App;
