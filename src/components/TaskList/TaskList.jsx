import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

import { Content as Content_mdl, Layout, Grid, Navigation, Drawer, ProgressBar } from 'react-mdl';


import Task from '../Task';

const TaskList = ({
  loading,
  tasks,
  onAddItem
}) => (

  <div>
    { loading && <ProgressBar indeterminate style={{width: 'auto'}} /> }
    <Grid className="demo-grid-1">

      {
        tasks.map( task => (
          <Task
            key={uuid.v4()}
            onAddItem={onAddItem}
            {...task}
          />
        ))
      }

     </Grid>
  </div>

);

TaskList.propTypes = {
  loading: PropTypes.bool.isRequired,
  tasks: PropTypes.arrayOf(PropTypes.object).isRequired,
  onAddItem: PropTypes.func
}

export default TaskList;
