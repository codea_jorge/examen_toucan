import React from 'react';
import PropTypes from 'prop-types';
import { Button, Dialog, DialogTitle, DialogActions, DialogContent, Textfield, Grid, Cell, col } from 'react-mdl';
import { MultiSelectField, Option } from 'react-mdl-extra';


const AddTaskDialog = ({ openDialog, onClose, onAddTask }) => (

  <div>
    <Dialog open={openDialog}>
      <DialogTitle>Nueva Tarea</DialogTitle>

        <Grid>
          <DialogContent>

            <Cell col={12}>
              <Textfield
                onChange={() => {}}
                label="Nombre"
                style={{width: '200px'}}
              />
            </Cell>

            <Cell col={12}>
              <Textfield
                onChange={() => {}}
                label="Descripcion"
                style={{width: '200px'}}
              />
            </Cell>

          </DialogContent>
        </Grid>

      <DialogActions>
        <Button
          type='button'
          onClick={() => onAddTask()}>Guardar</Button>
        <Button
          type='button'
          onClick={() => onClose()}>Cancelar</Button>
      </DialogActions>
    </Dialog>

  </div>

)

AddTaskDialog.propTypes = {
  openDialog: PropTypes.bool,
  onClose: PropTypes.func,
  onAddTask: PropTypes.func
}

export default AddTaskDialog;
