import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Content as Content_mdl, Layout, Grid, Navigation, Drawer, ProgressBar } from 'react-mdl';

// Components
import TaskList from '../TaskList';
import Timer from '../Timer';
//Actions
import * as taskActions from '../../actions/taskActions';
import * as timeActions from '../../actions/timeActions';

class TaskListContainer extends Component {

  constructor (props, context){
    super(props, context);

    this.handleOnAddItem = this.handleOnAddItem.bind(this);

  }

  async componentWillMount(){
    await this.props.taskActions.fetchTasks();
  }

  handleOnAddItem (item) {
    this.props.timeActions.addTimeItem(item);
  }

  render(){
    return (

      <Content_mdl>

          <Timer />

          <TaskList
            loading={this.props.loading}
            tasks={this.props.tasks}
            onAddItem={this.handleOnAddItem}
          />

      </Content_mdl>


    )
  }

}

TaskListContainer.defaultProps = {
  tasks: []
}

TaskListContainer.propTypes = {
  tasks:  PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool.isRequired,
  taskActions: PropTypes.objectOf(PropTypes.func).isRequired,
  timeActions: PropTypes.objectOf(PropTypes.func).isRequired,
}

function mapStateToProps (state) {
  return {
    tasks: state.taskList.tasks,
    loading: state.taskList.loading
  };
}

function mapDispatchToProps (dispatch) {
  return {
    taskActions: bindActionCreators(taskActions, dispatch),
    timeActions: bindActionCreators(timeActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskListContainer);
