import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import taskList from './taskListReducer';
import time from './timeReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  taskList,
  time
});

export default rootReducer;
