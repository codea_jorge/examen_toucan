import {
  LOAD_TIME_ITEMS,
  ADD_TIME_ITEM,
  REMOVE_TIME_ITEM
} from '../actions/types';

import initialState from './initialState';

export default function timeReducer ( state = initialState.time, action ) {
  switch (action.type) {
    case LOAD_TIME_ITEMS:
      return {
        ...state,
        item: [...state.item]
      };

      case ADD_TIME_ITEM:
        return {
          ...state,
          item: action.payload
        };

      case REMOVE_TIME_ITEM: {
        const searchItem = (elem) => elem.id === action.payload;
        const item = state.item.find(searchItem);
        const index = state.item.findIndex(searchItem);

        return {
          ...state,
          item: [
            ...state.item.slice(0, index),
            ...state.item.slice(index + 1)
          ]
        }

      }

    default:
      return state;

  }
}
