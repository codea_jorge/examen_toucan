const initialState = {
  taskList: {
    tasks: [],
    error: null,
    loading: false
  },

  time: {
    item: {}
  }

}
export default initialState;
