import {
  FETCH_TASKS_INIT,
  FETCH_TASKS_SUCCESS,
  FETCH_TASKS_FAILURE,
  SAVE_TASK_INIT,
  SAVE_TASK_SUCCESS,
  SAVE_TASK_FAILURE
} from '../actions/types';

import initialState from './initialState';

export default function taskListReducer (state = initialState.taskList, action) {
  switch (action.type) {
    case FETCH_TASKS_INIT:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_TASKS_FAILURE:
      return {
        ...state,
        tasks: [],
        error: action.payload,
        loading: true
      }
    case FETCH_TASKS_SUCCESS:
      return {
        ...state,
        tasks: action.payload,
        loading: false,
        error: null
      }

    default:
    return state;

  }
}
