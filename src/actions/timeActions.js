import {
  LOAD_TIME_ITEMS,
  ADD_TIME_ITEM,
  REMOVE_TIME_ITEM
} from './types';

export function loadTimeItems () {
  return {
    type: LOAD_TIME_ITEMS
  };
}

export function addTimeItem (item) {
  return {
    type: ADD_TIME_ITEM,
    payload: item
  };
}

export function removeTimeItem (itemId) {
  return {
    type: REMOVE_TIME_ITEM,
    payload: itemId
  };
}
