import {
  FETCH_TASKS_INIT,
  FETCH_TASKS_SUCCESS,
  FETCH_TASKS_FAILURE,
  FETCH_TASK_INIT,
  FETCH_TASK_SUCCESS,
  FETCH_TASK_FAILURE,
  SAVE_TASK_INIT,
  SAVE_TASK_SUCCESS,
  SAVE_TASK_FAILURE
} from './types';

import API from '../api';

export function fetchTasksSuccess (tasks) {
  return {
    type: FETCH_TASKS_SUCCESS,
    payload: tasks
  }
}

export function fetchTasksFailure (error) {
  return {
    type: FETCH_TASKS_FAILURE,
    payload: error
  }
}
export function fetchTaskSuccess (task) {
  return {
    type: FETCH_TASK_SUCCESS,
    payload: task
  }
}

export function fetchTaskFailure (error) {
  return {
    type: FETCH_TASK_FAILURE,
    payload: error
  }
}
export function saveTaskSuccess (task) {
  return {
    type: SAVE_TASK_SUCCESS,
    payload: task
  }
}

export function saveTaskFailure (error) {
  return {
    type: SAVE_TASK_FAILURE,
    payload: error
  }
}

// Actions Creators (Async)
export function fetchTasks () {
 return async (dispatch) => {
   dispatch(() => {
     return {
       type: FETCH_TASKS_INIT
     };
   });

   try {
     const data = await API.tasks.getAll();
     return dispatch(fetchTasksSuccess(data.data));
   } catch (error) {
     return dispatch(fetchTasksFailure(error));
   }
 };
}
