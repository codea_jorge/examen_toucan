import React from 'react';

import { Navigation, Header as Header_mdl , Textfield, HeaderRow } from 'react-mdl';

// Components

const Header = () => (
  <Header_mdl  hideTop>
    <HeaderRow title="Lista de tareas">
         <Textfield
             value=""
             onChange={() => {}}
             label="Search3"
             expandable
             expandableIcon="search"
         />
     </HeaderRow>

  </Header_mdl>
)

export default Header;
