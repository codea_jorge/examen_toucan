import React, { Component } from 'react';
import { FABButton, Icon } from 'react-mdl';

import AddTaskDialog from '../components/tasks/AddTask/AddTaskDialog';

const styles  = {
  FABButtonZIndex : {
    position: 'fixed',
    display: 'block',
    right: '0',
    bottom: '0',
    marginRight: '40px',
    marginBottom: '40px',
    zIndex: '900'
  }
}

class Fabbutton extends Component {

  constructor (props) {
    super (props);

    this.state = {
      open: false
    }

    this.handleOpenDialog = this.handleOpenDialog.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleSaveTask = this.handleSaveTask.bind(this);
  }

  handleOpenDialog(){

    this.setState({
      open: true
    })
    console.log('me estoy ejecutando')

  }

  handleCloseDialog() {
    this.setState({
      open: false
    })
  }

  handleSaveTask() {
    console.log('Guardar tareas')

    var a = [{name: 'jorge', edad: 23}, {name: 'gareth', edad: 3}]

    localStorage.setItem('task', 'nose' );
  }

  render() {
    return(
      <div>
        <FABButton colored ripple style={styles.FABButtonZIndex}
          onClick={this.handleOpenDialog} >
            <Icon name="add" />
        </FABButton>

        <AddTaskDialog
          openDialog={this.state.open}
          onClose={this.handleCloseDialog}
          onAddTask={this.handleSaveTask}
         />
      </div>
    )
  }

}

export default Fabbutton;
