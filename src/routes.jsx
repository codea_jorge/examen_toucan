import React from 'react';
import { Route, IndexRoute } from 'react-router';

// Components
import App from './components/App';
import TaskListContainer from './components/Containers/TaskListContainer';
import Timer from './components/Timer';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={TaskListContainer} />
    <Route path="/time" component={Timer} />
  </Route>
)
