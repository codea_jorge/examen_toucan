import fetch from 'isomorphic-fetch';

const baseURL = 'http://localhost:9000/examen/api/v1.0';

const API = {
  tasks: {
    async getAll () {
      const response = await fetch(`${baseURL}/tasks`);
      const data = await response.json();
      return data;
    }
  }
};

export default API;
